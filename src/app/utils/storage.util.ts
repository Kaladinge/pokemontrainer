export class StorageUtil {
  /**
   * Method to save to browsers session storage.
   * @param key key to store
   * @param value value to store
   */
  public static storageSave<T>(key: string, value: any): void {
    sessionStorage.setItem(key, JSON.stringify(value));
  }

  /**
   * This method retrieves value stored in session storage
   * @param key key for retrieving data
   * @returns value stored in session storage
   */
  public static storageRead<T>(key: string): T | undefined {
    const storedValue = sessionStorage.getItem(key);
    try {
      if (storedValue) {
        return JSON.parse(storedValue) as T;
      }
      return undefined;
    } catch (error) {
      sessionStorage.removeItem(key);
      return undefined;
    }
  }
}
