import { Component, EventEmitter, Output } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { NgForm } from '@angular/forms';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent {
  @Output() login: EventEmitter<void> = new EventEmitter();

  constructor(
    private readonly loginService: LoginService,
    private readonly trainerService: TrainerService,
  ) {}

  /**
   * This method takes input from user and logs 
   * user in
   * @param loginForm the input to be read
   */
  public loginSubmit(loginForm: NgForm): void {
    const { username } = loginForm.value;
    this.loginService.login(username).subscribe({
      next: (trainer: Trainer) => {
        this.trainerService.trainer = trainer; 
        this.login.emit(); 
      },
      error: (error) => {
        console.log('ERROR', error.message);
      },
    });
  }
}
