import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonDetails } from 'src/app/models/pokemondetails.model';
import { PokemonDetailsService } from 'src/app/services/pokemon-details.service';

@Component({
  selector: 'app-show-more-button',
  templateUrl: './show-more-button.component.html',
  styleUrls: ['./show-more-button.component.scss'],
})
export class ShowMoreButtonComponent implements OnInit {
  @Input() pokemon?: Pokemon;
  public modal: boolean = false;

  get pokemonDetails(): PokemonDetails | undefined {
    return this.pokemonDetailsService.pokemonDetails;
  }

  constructor(private readonly pokemonDetailsService: PokemonDetailsService) {}

  ngOnInit(): void {}

  public onShowClick(): void {
    this.modal = !this.modal;
    if (this.pokemon)
      this.pokemonDetailsService.findPokemonDetails(this.pokemon.id);
  }

  public onCloseModalClick(): void {
    this.modal = !this.modal;
  }
}
