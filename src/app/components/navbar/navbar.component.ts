import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  get trainer(): Trainer | undefined{
    return this.trainerService.trainer; 
  }

  constructor(
    private readonly trainerService: TrainerService, 
    private router: Router  
  ) { }

  ngOnInit(): void {
  }

  /**
   * This method cleares session storage and log user out. 
   * Redirects to login-page
   */
  public logOut() {
    sessionStorage.clear(); 
    this.trainerService.trainer = undefined; 
    this.router.navigate(["/login"]); 
  }
}
