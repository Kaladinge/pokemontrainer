import { Ability } from "./ability.model";

export interface PokemonDetails {
    abilities: Ability[]; 
    types: Type[]; 
    stats: StatObject[]; 
}

export interface Type {
    type: TypeObject; 
}

export interface TypeObject {
    name: string; 
}

export interface StatObject {
    base_stat: number; 
    effort: number; 
    stat: Stat; 
}

export interface Stat {
    name: string
}