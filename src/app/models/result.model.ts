import { Pokemon } from "./pokemon.model";

export interface Result {
    count: number,
    next: string,
    previous: string,
    results: Pokemon[];
}