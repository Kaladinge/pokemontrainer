import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PokemonDetails } from '../models/pokemondetails.model';

const { apiDetails } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonDetailsService {
  private _pokemonDetails: PokemonDetails | undefined = undefined;

  get pokemonDetails(): PokemonDetails | undefined {
    return this._pokemonDetails;
  }

  constructor(private http: HttpClient) {}

  /**
   * Method to find all pokemon details to be exposed to user.
   * @param pokemonId pokemon id to fetch details for.
   */
  public findPokemonDetails(pokemonId: string) {
    const test = apiDetails + pokemonId.replace('#', '');
    console.log(test);

    this.http
      .get<PokemonDetails>(test)
      .pipe(
        finalize(() => {
          console.log(this._pokemonDetails);
        })
      )
      .subscribe({
        next: (result: PokemonDetails) => {
          //console.log(result);
          this._pokemonDetails = result;
        },
        error: (error: HttpErrorResponse) => {
          console.log(error.message);
        },
      });
  }
}
